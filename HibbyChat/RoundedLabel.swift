//
//  RoundedButton.swift
//  Hippy
//
//  Created by Mostafa on 5/14/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit

class RoundedLabel: UILabel {
    
    override func draw(_ rect: CGRect) {
        let maskLayer = CAShapeLayer.init()
        var corners : UIRectCorner = UIRectCorner.allCorners;
            corners = UIRectCorner.topRight.union(UIRectCorner.bottomRight).union(UIRectCorner.topLeft.union(UIRectCorner.bottomLeft))
            
        maskLayer.path = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize.init(width: 20, height: 20)).cgPath
        
        self.layer.mask = maskLayer;
        super.draw(rect)
        
    }
}
