//
//  ChatMessageCellDelegate.swift
//  HibbyChat
//
//  Created by Jihad Ismail on 10/12/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation

protocol ChatMessageCellDelegate {
    func openUser(_ user: User)
}
