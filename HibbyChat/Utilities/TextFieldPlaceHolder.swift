//
//  TextFieldPlaceHolder.swift
//  Hippy
//
//  Created by Mostafa on 6/12/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit

class TextFieldPlaceHolder: UITextField {
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                                                                          attributes: [NSForegroundColorAttributeName: UIColor.init(red: 38.0/255.0, green: 38.0/255.0, blue: 38.0/255.0, alpha: 0.42)])
    }
}
