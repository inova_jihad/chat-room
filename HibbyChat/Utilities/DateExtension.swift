//
//  DateExtention.swift
//  Hippy
//
//  Created by Mostafa on 5/18/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import DateToolsSwift
extension Date {
    
    
    static func getFirebaseRepresentation() -> String {
        let date = Date()
        
        if let utcCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian) {
            if let utcTimeZone = NSTimeZone(abbreviation: "UTC") {
                
                utcCalendar.timeZone = utcTimeZone as TimeZone
                
                let ymdhmsUnitFlags: NSCalendar.Unit = [.hour,.minute,.second,.day,.month,.year]
                print(ymdhmsUnitFlags.contains(.hour))
                    
                
                let utcDateComponents = utcCalendar.components(ymdhmsUnitFlags, from: date)
                
                // Create string of form "yyyy-mm-dd hh:mm:ss"
                let utcDateTimeString = NSString(format: "%02u:%02u:%02u %02u-%02u-%04u",
                                                 UInt(utcDateComponents.hour!),
                                                 UInt(utcDateComponents.minute!),
                                                 UInt(utcDateComponents.second!),
                                                 UInt(utcDateComponents.month!),
                                                 UInt(utcDateComponents.day!),
                                                 UInt(utcDateComponents.year!))
                return utcDateTimeString as String
            }
        }
            return ""
    }
    
    func getDateRep() -> String {
        let now = Date()
        
        if now < self {
            return self.timeAgoSinceNow
        }
        var result = ""
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current

        if self.isToday {
            // Check If withing this hour
            let minDiff = now.timeIntervalSince(self)
            if minDiff < 60 {
                return "now"
            }
            if minDiff < 60*60 {
                return "\(Int(minDiff/60.0)) min ago"
            }
            
            return "\(Int(minDiff/3600.0)) h ago"

        }else{
            if self.isYesterday {
                result = result + "Yesterday"
                dateFormatter.dateFormat = " 'at' hh:mm a"
                
            }else{
                if self >= now.start(of: .year) {
                    dateFormatter.dateFormat = "MMM dd 'at' hh:mm a"
                }else{
                    dateFormatter.dateFormat = "MMM dd yyyy 'at' hh:mm a"
                }
            }
        }
        result = result + dateFormatter.string(from: self)

//        let rep = self.timeAgoSinceNow
//        if rep.contains("second") {
//            return "now"
//        }
//        return rep
//
        return result
    }
    
    func getUTC() -> Date {
        return self
    }
}
