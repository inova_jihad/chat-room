//
//  UserProfileImageView.swift
//  Hippy
//
//  Created by inova5 on 7/20/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit


class UserProfileImageView: UIView {
    @IBOutlet var profileImageView: UIImageView!
    var delegate : UserProfileImageViewDelegate?
    @IBAction func dismissView(_ sender: UITapGestureRecognizer) {
        self.delegate?.closeView()
    }
}
