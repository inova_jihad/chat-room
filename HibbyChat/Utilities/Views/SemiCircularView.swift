//
//  SemiCircularView.swift
//  Hippy
//
//  Created by inova5 on 6/14/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit

class SemiCircularView: UIView {
    override func draw(_ rect: CGRect) {
        let maskLayer = CAShapeLayer.init()
        let corners : UIRectCorner = UIRectCorner.allCorners;
        //        if self.tag == 0 {
        //            corners = UIRectCorner.topRight.union(UIRectCorner.bottomRight)
        //
        //        }else if self.tag == 1 {
        //            corners = UIRectCorner.topLeft.union(UIRectCorner.bottomLeft)
        //        }
        maskLayer.path = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize.init(width: 20, height: 20)).cgPath
        
        self.layer.mask = maskLayer;
        
    }
}
