//
//  StringDate.swift
//  Hippy
//
//  Created by Mostafa on 5/18/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation

extension String {
    func parseServerDate() -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        /* date_format_you_want_in_string from
         * http://userguide.icu-project.org/formatparse/datetime
         */
        dateFormatter.timeZone = TimeZone.init(identifier: "UTC")
        if let date = dateFormatter.date(from: self)
        {
            return date
        }
        return Date()
    }
    
    func parseFirebaseDate() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:SS MM-dd-yyyy"
        /* date_format_you_want_in_string from
         * http://userguide.icu-project.org/formatparse/datetime
         */
        dateFormatter.timeZone = TimeZone.init(identifier: "UTC")
        if dateFormatter.date(from: self) != nil{
            
            if let date = dateFormatter.date(from: self)
            {
                return date
            }
        }
        return Date()
    }
}
