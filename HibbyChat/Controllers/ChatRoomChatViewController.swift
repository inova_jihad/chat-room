//
//  ChatRoomChatViewController.swift
//  HippyChat
//
//  Created by Mostafa on 5/17/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import AWSCore
import AWSS3
import Photos
import SDWebImage

class ChatRoomChatViewController: AbstractScrollableViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,SelectUsersDelegate, UITableViewDelegate,UITableViewDataSource,ChatMessageCellDelegate {
    
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var sendMessageButton: RoundedButton!
    @IBOutlet weak var chatTableView: UITableView!
//    @IBOutlet weak var dimView: UIView!
    @IBOutlet weak var cancelButton: RoundedButton!
    
    @IBOutlet weak var messageImageView: UIImageView!
    
    @IBOutlet weak var chatRoomTitle: UILabel!
    
    var state : State = .SendMessage
    var imagePickerVC: UIImagePickerController!
    
    var currentUser:User = User()
    var chatHistory : [ChatMessage] = []
    var chatRoom : ChatRoom = ChatRoom()
    
    var selectedImageUrl: NSURL!
    var selectedImage : UIImage?
    var imageUrl : String = ""
    var image_uploaded : Bool = false
    var selectedMsg : ChatMessage?

    
    var chatManager : ChatManager = ChatManager()
    
    enum State {
        case SendMessage
    }
    
    func openUser(_ user: User) {
//        self.parentGroup?.showUserProfile(user)
    }
    
    
    func chatCount() -> Int {
        return self.chatHistory.count
    }
    
    
    override func viewDidLoad() {
        
        scrollViewMargin = 0.0
        self.can_show_group = true
        set_background = true
        self.chatRoomTitle.text = self.chatRoom.title
        super.viewDidLoad()
        self.reloadData()
        
    }

    
    func reloadData(){
        currentUser = UserManager().loadCachedUser()!
        
        self.loadChat()
        self.sendMessageButton.isHidden = false
    }
    
    func loadChat(){
        if self.checkConnection(){
            self.showLoading()
        
            chatManager.getChatMessages(chatRoom: self.chatRoom) { (response) in
            
                self.hideLoading()
            
                if let messages : [ChatMessage] = response?.results as? [ChatMessage]{
                
                    self.chatHistory = messages.sorted(by: { (first, second) -> Bool in
                    
                        return second.date.parseFirebaseDate() > first.date.parseFirebaseDate()
                    })
                
                    self.chatTableView.reloadData()
        
                    if self.chatHistory.count > 0 {
                    
                        DispatchQueue.main.async {
                        self.chatTableView.scrollToRow(at: IndexPath.init(row: self.chatHistory.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
                        }
                    }
                
                    ChatRoomManager().visitChatRoom(chatRoom: self.chatRoom, handler: { (response) in
                    
                    })

                    self.hideLoading()
 
                }
            }
        }
    }
    
    @IBAction func cancelSendingImage(_ sender: UIButton) {
            self.cancelButton.isHidden = true
            self.messageImageView.isHidden = true
            self.chatTableView.superview?.bringSubview(toFront: self.chatTableView )
//            self.dismissKeyboard()
            self.imageUrl = ""
            self.image_uploaded = false
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
       self.dismiss(animated: true, completion: nil)
    }
    
    
    override func keyboardWasShown(note: NSNotification!) {
        self.sendMessageButton.isHidden = false
        
//        DispatchQueue.main.async {
//            self.chatTableView.superview?.sendSubview(toBack: self.chatTableView)
//        }
        
        super.keyboardWasShown(note: note)
        
        if self.image_uploaded {
            self.messageImageView.isHidden = false
            self.messageImageView.sd_setImage(with: URL(string: self.imageUrl), placeholderImage: #imageLiteral(resourceName: "imgPlaceholder"))

        }
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.chatTableView.superview?.bringSubview(toFront: self.scrollView )
        return super.textFieldShouldReturn(textField)
    }
    
    override func keyboardWillBeHidden(note: NSNotification!) {
        
        if self.state == .SendMessage{
            self.chatTableView.superview?.bringSubview(toFront: self.chatTableView)
            super.keyboardWillBeHidden(note: note)
//            self.dimView.isHidden = true
            self.sendMessageButton.isHidden = false
            self.cancelButton.isHidden = true
            self.scrollView.isScrollEnabled = false
        }
    }
    
    // MARK: User Actions

    @IBAction func sendMessagePressed(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.cancelButton.isHidden = true
        }
        if self.checkConnection(){

            let chatMessage = ChatMessage()
        
            chatMessage.message = self.messageTextField.text!
            chatMessage.image_url = self.imageUrl
        
            self.resetMessageData()

            if chatMessage.message.characters.count == 0 && chatMessage.image_url.characters.count == 0 {
                showToastMessage(messag: "Can't send empty message")
                return
            }
        
            self.showLoading()
        
            chatMessage.date = Date.getFirebaseRepresentation()
            chatMessage.user_id = currentUser.id
            chatMessage.chat_room_id = chatRoom.id
        
            self.scrollView.contentOffset = CGPoint.zero

            
            ChatManager().sendChat(chatMessage: chatMessage, chatRoom: self.chatRoom, handler: { (result) in
                self.hideLoading()
                // TODO Msg Sent
                if (result?.status)! {
                    self.chatTableView.superview?.bringSubview(toFront: self.chatTableView)
                    ChatRoomManager().chatMsgSent(chatMessage: chatMessage, handler: { (response) in
                    
                    })
                }
            })
        }
    }
    
    
    func resetMessageData(){
        // Reset Message Fields
        self.dismissKeyboard()
        self.messageTextField.text = ""
        self.imageUrl = ""
        self.image_uploaded = false
        self.messageImageView.isHidden = true
    }
    
    // MARK: Send Hippy Logic
    @IBAction func captureImagePressed(_ sender: UIButton) {
        self.chatTableView.superview?.bringSubview(toFront: self.scrollView)
        self.state = .SendMessage
        self.openImagePicker()
    }
    
    func openImagePicker(){
        print("Capture Image")
        let alertVC = UIAlertController(title: StoryBoardConstants.mediaActionSheetTitle, message: StoryBoardConstants.mediaActionSheetMessage, preferredStyle: .actionSheet)
        alertVC.addAction(UIAlertAction(title: StoryBoardConstants.cameraActionButtonTitle, style: .default, handler: { (action) in
            self.openImagePickerViewCamera()
        }))
        alertVC.addAction(UIAlertAction(title: StoryBoardConstants.galleryActionButtonTitle, style: .default, handler: { (action) in
            self.openImagePickerViewGallery()
        }))
        alertVC.addAction(UIAlertAction(title: StoryBoardConstants.cancelActionButtonTitle, style: .cancel, handler: nil))
        present(alertVC, animated: true, completion: nil)

    }
    
    
    /*
     *  private function that presents image picker from gallery controller
     */
    private func openImagePickerViewGallery()
    {
        let status = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        switch status {
        case .authorized:
            break
        case .denied:
            showToastMessage(messag: "Media Not Allowed")
            handleError(error: ModelErrors.MediaNotAllowed)
            return
        default:
            break
        }
        
        imagePickerVC = UIImagePickerController()
        imagePickerVC.delegate = self
        imagePickerVC.sourceType = .savedPhotosAlbum
        imagePickerVC.allowsEditing = true
        present(imagePickerVC, animated: true, completion: nil)
        
    }
    
    /*
     *  private function that presents image picker from camera controller
     */
    private func openImagePickerViewCamera()
    {
        let status = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        switch status {
        case .authorized:
            break
        case .denied:
            showToastMessage(messag: "Camera Not Allowed")
            handleError(error: ModelErrors.CameraNotAllowed)
            return
        default:
            break
        }
        
        imagePickerVC = UIImagePickerController()
        imagePickerVC.delegate = self
        imagePickerVC.sourceType = .camera
        imagePickerVC.allowsEditing = true
        present(imagePickerVC, animated: true, completion: nil)
        
    }
    
    
    
    /*
     *  MARK: UIImagePickerController delegate function.
     */

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        imagePickerVC.dismiss(animated: true, completion: nil)
        DispatchQueue.main.async {
            if let _ = info[UIImagePickerControllerReferenceURL] as? NSURL, let _ = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.selectedImageUrl = info[UIImagePickerControllerReferenceURL] as! NSURL
            self.selectedImage = info[UIImagePickerControllerEditedImage] as? UIImage
            }else {
                self.selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
            }
            self.startUploadImage()
        }
    }
    
   
    func startUploadImage()
    {
        self.showLoading()
        // MARK: - AWS S3 Configuration
        let s3Creds = AWSStaticCredentialsProvider(accessKey: "AKIAJA3G6RUBWJBVAVKQ", secretKey: "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")
        let configuration = AWSServiceConfiguration(region: AWSRegionType.EUWest1, credentialsProvider: s3Creds)
        
        AWSS3TransferManager.register(with: configuration!, forKey: "defaultTransferManager")
        
        // Set up AWS Transfer Manager Request
        let uuid = UUID().uuidString
        let S3BucketName = "statsquo-contentdelivery-mobilehub-1136698527"
        let remoteName = "\(uuid).jpeg"
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        do{
            try uploadRequest?.body = generateImageUrl(fileName: remoteName) as URL!
        }catch {
            self.hideLoading()
            print("error in creating temp file")
        }
        uploadRequest?.key = "images/\(remoteName)"
        uploadRequest?.bucket = S3BucketName
        uploadRequest?.contentType = "image/jpeg"
        //AWSS3ObjectCannedACLPublicRead
        uploadRequest?.acl = .publicRead
        
        // Perform file upload
        //         let transferManager = AWSS3TransferManager.default()
        let transferManager = AWSS3TransferManager.s3TransferManager(forKey: "defaultTransferManager")
        transferManager.upload(uploadRequest!).continueWith { (task) -> Any? in
            
            if let error = task.error {
                print("Upload failed with error: (\(error.localizedDescription))")
            }
            
            if task.result != nil {
                
                let s3URL = NSURL(string: "https://\(S3BucketName).s3.amazonaws.com/\(uploadRequest!.key!)")!
                print("Uploaded to:\n\(s3URL)")
                self.imageUrl = "https://\(S3BucketName).s3.amazonaws.com/\(uploadRequest!.key!)"
                // Remove locally stored file
                //       self.remoteImageWithUrl(uploadRequest.key!)
                self.image_uploaded = true
                
                if self.selectedImage != nil{
                     DispatchQueue.main.async {
                        self.cancelButton.isHidden = false
                    
                        self.messageImageView.isHidden = false
                        self.messageImageView.sd_setImage(with: URL(string: self.imageUrl), placeholderImage: #imageLiteral(resourceName: "imgPlaceholder"))
                    }
                }
                
            }
            else {
                print("Unexpected empty result.")
            }
            self.hideLoading()
            return nil
        }
        
        
    }
    
    func generateImageUrl(fileName: String) throws -> NSURL
    {
        let fileURL = NSURL(fileURLWithPath: NSTemporaryDirectory().appendingFormat(fileName))
        let data = UIImageJPEGRepresentation(self.selectedImage!, 0.6)
        try data?.write(to: fileURL as URL)
        return fileURL
    }
    
    // MARK: Controllers
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "selectUsersSegue" {
            if let dest = segue.destination as? SelectUsersViewController {
                dest.groupType = .Group
                dest.chatRoomImageURL = self.imageUrl
                dest.chatRoom_title = self.chatRoom.title
                dest.delegate = self
                
            }
        }else if segue.identifier == "previewChatSegue" {
            if let dest  = segue.destination as? ChatDetailViewController {
                dest.chatMsg = self.selectedMsg
                var chatUser = UserManager().loadCachedUser()
                for user in self.chatRoom.users {
                    if selectedMsg?.user_id == user.id {
                        chatUser = user
                    }
                }
                dest.user = chatUser!
            }
        }
    }

    // MARK: Select Users Delegate
    func didFinish(success: Bool) {
        
        // TODO
        self.cancelButton.isHidden = true
        self.chatTableView.superview?.bringSubview(toFront: self.chatTableView)
        if success {
            self.sendMessageButton.isHidden = false
        }
        self.dismissKeyboard()
        self.state = .SendMessage
        
        if success {

            showToastMessage(messag: "Group has been created Successfully")

            self.messageTextField.text = ""
            self.messageImageView.image = nil
            self.messageImageView.isHidden = true

            self.selectedImageUrl = nil
            self.selectedImage = nil
        }
        
    }
    
    func showMessageInp(){
        // TODO
        // Show
//        self.dimView.isHidden = false
        self.cancelButton.isHidden = false
        self.messageTextField.becomeFirstResponder()
        self.scrollView.isHidden = true
    }
    
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatHistory.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let chatMsg = self.chatHistory[indexPath.row]
        let height = AbstractChatMessageCell.heightForLabel(text: chatMsg.message, width: self.view.frame.width - 103)
        if chatMsg.image_url == "" {
            return height - 129.5
        }
        return height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var key = "ChatMessageOther"
        let chatMsg = self.chatHistory[indexPath.row]

        switch chatMsg.type {
        case 1:
            key = "ChatMessageOther"
            if chatMsg.user_id == currentUser.id {
                // TODO Self
                key = "ChatMessageUser"

            }
            break
        default:
            key = "ChatMessageSystem"
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: key, for: indexPath) as! AbstractChatMessageCell
        
        cell.delegate = self
        
        var msgUser = currentUser
        
        for user in self.chatRoom.users {
            if user.id == chatMsg.user_id{
                msgUser = user
            }
        }
        
        cell.bindMsg(msg: chatMsg, user: msgUser)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatMsg = self.chatHistory[indexPath.row]
        
        if URL.init(string: chatMsg.image_url) != nil {
            // TODO
            self.selectedMsg = chatMsg
            performSegue(withIdentifier: "previewChatSegue", sender: nil)
        }
    }
    
}
