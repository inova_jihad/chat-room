//
//  ChatRoomsViewController.swift
//  HippyChat
//
//  Created by Mostafa on 5/16/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit

class ChatRoomsViewController: AbstractViewController,UITableViewDelegate,UITableViewDataSource, ChatRoomCellDelegate {
    
    @IBOutlet weak var noResultView: UIView!
    @IBOutlet weak var chatRoomsTableView: UITableView!
    @IBOutlet weak var chatRoomTitleTextField: UITextField!
    @IBOutlet weak var createChatRoomBKGImage: UIImageView!

    @IBOutlet weak var createchatRoomView: UIView!
    
    @IBOutlet weak var createChatRoomView: UIView!
    
    var chatRooms : [ChatRoom] = []
    var selectedChatRoom : ChatRoom?
    var chatRoomTitle : String = ""

    func openUser(_ user: User) {
        self.showUserProfile(user)
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ChatRoomsViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        // Simply adding an object to the data source for this example
        self.loadChatRooms()
        chatRoomsTableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    override func viewDidLoad() {
        set_background = false
        super.viewDidLoad()
        let userManager = UserManager()
        userManager.registerDeviceToken(deviceToken: userManager.readNotifToken())
        Constants.navController = self.navigationController
        self.chatRoomsTableView.addSubview(refreshControl)
        Constants.chatRoomsView = self

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadChatRooms()
    }
    
    // MARK: Table View Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.chatRooms.count > 0 {
            self.noResultView.isHidden = true
        }else{
            self.noResultView.isHidden = false
        }
        return self.chatRooms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatRoomCell", for: indexPath) as! ChatRoomCell
        cell.delegate = self
        cell.bindChatRoom(chatRoom: self.chatRooms[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //TODO
        print("Did Select ChatRoom")
        let chatRoom = self.chatRooms[indexPath.row]
        self.chatRoomTitle = chatRoom.title
        self.selectedChatRoom = chatRoom
        performSegue(withIdentifier: "showChatRoom", sender: nil)
    }
    

    func loadChatRooms(){
        if self.checkConnection(){
            self.showLoading()
            let currentUser = UserManager().loadCachedUser()
            if let user = currentUser {
                ChatRoomManager().getUserChatRooms(user: user) { (response) in
                
                    if response.status {
                        if let chatRoomsArray = response.results as? [ChatRoom] {
                            self.chatRooms = chatRoomsArray
                            if self.chatRooms.count == 0{
                            }
                        }else{
                            self.chatRooms = []
                        }
                    }else{
                        if let error = response.error {
                            self.showToastMessage(messag: error.localizedDescription)
                        }else{
                            self.showToastMessage(messag: "Failed To Load Chat Rooms")
                        }
                        self.chatRooms = []
                    }
                    self.chatRoomsTableView.reloadData()
                    self.hideLoading()
                }
            }else{
                self.showToastMessage(messag: "Reauthenticate")
            }
        }
    }
    
    // MARK: Create chatRoom
    @IBAction func createChatRoomButtonPressed(_ sender: UIButton) {
        
        view.layoutIfNeeded()
        DispatchQueue.main.async {
            self.createChatRoomView.frame = CGRect(origin:CGPoint(x: self.view.frame.minX, y: self.view.frame.height)
                , size: CGSize(width: self.view.frame.width, height: 0))
            
            UIView.animate(withDuration: 0.1, animations: {
                self.createChatRoomView.frame = CGRect(origin:CGPoint(x: self.view.frame.minX, y: self.view.frame.minY)
                    , size: CGSize(width: self.view.frame.width, height: self.view.frame.height))
                
                //                self.view.layoutIfNeeded()
            })
            self.createChatRoomView.isHidden = false
            self.chatRoomTitleTextField.text = ""
            self.view.bringSubview(toFront: self.createChatRoomView)
            
        }
    }
    
    @IBAction func closeCreationView(_ sender: UITapGestureRecognizer) {
        let location = sender.location(in: self.createChatRoomBKGImage)
        
        if !self.createChatRoomBKGImage.bounds.contains(location){
            DispatchQueue.main.async {
                self.chatRoomTitleTextField.resignFirstResponder()
            }
            self.createChatRoomView.isHidden = true
        }
    }

    
    @IBAction func chatRoomTitleEntered(_ sender: UIButton) {
        if let size = self.chatRoomTitleTextField.text?.characters.count , size > 0 {
            self.view.sendSubview(toBack: self.createChatRoomView)
            self.createChatRoomView.isHidden = true
            self.createChatRoom(title: self.chatRoomTitleTextField.text!)
        }else{
            showToastMessage(messag: "Please enter a valid title")
        }
    }
    
    func createChatRoom(title: String){
        print("Create chat room With title \(title)")
        self.chatRoomTitle = title
        self.view.endEditing(true)
        self.performSegue(withIdentifier: "createChatRoomSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "createChatRoomSegue" {
            
            
            if let dest = segue.destination as? SelectUsersViewController {
                dest.chatRoom_title = self.chatRoomTitle
            }
        }else if segue.identifier == "showChatRoom" {
            if let dest = segue.destination as? ChatRoomChatViewController {

                if let chatRoom = self.selectedChatRoom {
                    dest.chatRoom = chatRoom
                }
            }
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}
