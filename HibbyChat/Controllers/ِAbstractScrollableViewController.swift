//
//  ِAbstractScrollableViewController.swift
//  Hippy
//
//  Created by Mostafa on 5/14/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit

class AbstractScrollableViewController: AbstractViewController, UITextFieldDelegate, UITextViewDelegate
{
    
    public var isKeyboardVisible = false
    public var defaultContentSize = CGSize.zero
    public var activeField: UIView!
    public var navigableItems : Array<UIView> = Array<UIView>()
    public var tapGesture : UITapGestureRecognizer!
    public var iNote: NSNotification!
    public var defaultContentOffset = CGPoint.zero
    public var margin: CGFloat = 0
    public var scrollViewMargin = 0.0
    
    @IBOutlet public weak var scrollView: UIScrollView!
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.scrollView.contentSize = CGSize(width: self.scrollView.subviews.first!.frame.size.width, height: self.scrollView.subviews.first!.frame.size.height + margin)//self.scrollView.subviews.first!.frame.size
        self.scrollView.setContentOffset(CGPoint.zero, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.scrollView != nil {
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown), name:NSNotification.Name.UIKeyboardDidShow, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidChange), name:NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.scrollView != nil {
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    public func dismissKeyboard() {
        self.activeField = nil
        self.view.endEditing(true)
        self.view.removeGestureRecognizer(self.tapGesture)
    }
    
    func keyboardWasShown(note: NSNotification!) {
        if note != nil {
            self.iNote = note
        }
        else if self.iNote == nil {
            return
        }
        
        if self.activeField != nil {
            var info:Dictionary = self.iNote.userInfo!
            let kbSize  = ((info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size)!
            if (!self.isKeyboardVisible) {
                self.isKeyboardVisible = true
                self.defaultContentSize =  CGSize(width: self.scrollView.frame.size.width, height: self.scrollView.frame.size.height + margin)//self.scrollView.frame.size
//                self.defaultContentOffset = self.scrollView.contentOffset
            }
            self.scrollView.contentSize = CGSize(width: self.defaultContentSize.width, height: defaultContentSize.height + kbSize.height)
            
            var topOfActiveField = self.activeField.convert(self.activeField.frame.origin, to: nil)
            //            var middleOfActiveField = CGPointMake(topOfActiveField.x, topOfActiveField.y + self.activeField.frame.size.height/2)
            let bottomOfActiveField = CGPoint(x: topOfActiveField.x, y: topOfActiveField.y + self.activeField.superview!.frame.size.height)
            var visibleRect = self.view.frame
            visibleRect.size.height -= kbSize.height;
            let yOffset = self.defaultContentOffset.y + max(kbSize.height, bottomOfActiveField.y - kbSize.height)
            let superView = self.activeField.superview!
            
            let visibleFrame = CGRect.init(x: 0, y: superView.frame.origin.y, width: superView.frame.width, height: superView.frame.height + kbSize.height - CGFloat(scrollViewMargin))
            if !visibleRect.contains(bottomOfActiveField) && self.scrollView.contentOffset.y < yOffset
            {
//                let scrollOffset = CGPoint(x: 0, y: yOffset)
//                self.scrollView.setContentOffset(scrollOffset, animated: true)
                self.scrollView.scrollRectToVisible(visibleFrame, animated: true)
                topOfActiveField = self.activeField.convert(self.activeField.frame.origin, to: nil)
            }
        }
    }
    
    func keyboardWillBeHidden(note: NSNotification!) {
        if self.activeField != nil {
            return
        }
        if (self.isKeyboardVisible) {
            isKeyboardVisible = false
            self.scrollView.contentSize = defaultContentSize
            self.scrollView.setContentOffset(self.defaultContentOffset, animated: true)
        }
    }
    
    func keyboardDidChange(note: NSNotification!) {
        if (self.isKeyboardVisible) {
            keyboardWasShown(note: note)
        }
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeField = textField
        self.keyboardWasShown(note: nil)
        self.view.addGestureRecognizer(self.tapGesture)
    }
    
    func indexOfCurrentField(view: UIView) -> Int {
        for i in 0 ..< self.navigableItems.count {
            if self.navigableItems[i] == view {
                return i
            }
        }
        return -1
    }
    
    public func handleItemNavigation (field : UIView) {
        let indexOfCurrentItem = indexOfCurrentField(view: field)
        if (indexOfCurrentItem > -1 && indexOfCurrentItem < navigableItems.count - 1) {
            self.activeField = self.navigableItems[indexOfCurrentItem+1]
            self.navigableItems[indexOfCurrentItem+1].becomeFirstResponder()
        }
        else {
            self.dismissKeyboard()
        }
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.handleItemNavigation(field: textField)
        if self.activeField is UITextField {
            return false
        }
        return true
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            self.handleItemNavigation(field: textView)
            return false
        }
        return true
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        //        activeField = nil
    }
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        self.activeField = textView
        self.keyboardWasShown(note: nil)
        self.view.addGestureRecognizer(self.tapGesture)
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        //        activeField = nil
    }
    
    
}
