//
//  AbstractViewController.swift
//  HippyChat
//
//  Created by Mostafa on 5/14/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import AWSCore
import AWSS3
import Photos
import Toast_Swift
import MBProgressHUD
import Crashlytics
import SDWebImage
import ReachabilitySwift


class AbstractViewController: UIViewController, UserProfileImageViewDelegate {
    
    static let statusBar_color = UIColor.init(red: 109.0/255.0, green: 178.0/255.0, blue: 33.0/255.0, alpha: 0.75)
    
    var set_background = true
    var can_show_group = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if set_background {
            self.setBackgroundImage()
        }
        self.setStatusBarBackground()
        
        if let views = Bundle.main.loadNibNamed("UserProfileImage", owner: self, options: nil) as? [Any?]{
            if let view = views.first as? UserProfileImageView {
                view.frame = self.view.frame
                self.view.addSubview(view)
                self.view.bringSubview(toFront: view)
                view.delegate = self
                view.isUserInteractionEnabled = true
                view.profileImageView.isUserInteractionEnabled = true
                self.userProfileView = view
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UserManager().registerDeviceToken(deviceToken: UserManager().readNotifToken())
        if self.can_show_group {
            Constants.currentView = self
        }
    }
    
//    func openGroup(groupID : Int){
//        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let initialViewControlleripad : GroupViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "GroupViewController") as! GroupViewController
//        initialViewControlleripad.group_id = groupID
//        self.navigationController?.pushViewController(initialViewControlleripad, animated: true)
//    }
    
    func showNotification(groupID : Int , notificationBody : String){
        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewControlleripad : NotificationViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        initialViewControlleripad.parentController = self
        initialViewControlleripad.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        initialViewControlleripad.notification_body = notificationBody
        self.present(initialViewControlleripad, animated: true) { 
            
        }
    }
    var userProfileView : UserProfileImageView?

    func shareApp(){
        let textToShare = "Hibby is awesome!  Check out this website about it!"
        
        if let myWebsite = URL(string: "https://itunes.apple.com/us/app/hibby/id1250626871?ls=1&mt=8") {
            let objectsToShare = [textToShare, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            self.present(activityVC, animated: true) {
                //self.trackGAIEventWithName("Share App", andScreenName: "Info View")
            }
        }else{
            showToastMessage(messag: "Invalid App URL")
        }
        
    }

    func showUserProfile(_ user: User){
        view.layoutIfNeeded()
        DispatchQueue.main.async {
            if let url = URL.init(string: user.image_url) {
            
            if let profileView = self.userProfileView {
            profileView.frame = CGRect(origin:CGPoint(x: self.view.frame.minX, y: self.view.frame.height)
                , size: CGSize(width: self.view.frame.width, height: 0))
            
            UIView.animate(withDuration: 0.1, animations: {
                profileView.frame = CGRect(origin:CGPoint(x: self.view.frame.minX, y: self.view.frame.minY)
                    , size: CGSize(width: self.view.frame.width, height: self.view.frame.height))
                
                //                self.view.layoutIfNeeded()
            })
                profileView.profileImageView.sd_setImage(with: url, placeholderImage: user.getPlaceholderImage())

            profileView.isHidden = false
            self.view.bringSubview(toFront: profileView)
                }
            }
        }

        
        
        
//        self.userProfileView.isHidden = false
//        self.profileImageView.isHidden = false
           }
    // MARK : Close User Profile Image
    
    
    func closeView() {
        self.userProfileView?.isHidden = true
    }
    
    @IBAction func closeProifleImage(_ sender: UITapGestureRecognizer) {
//        let location = sender.location(in: self.profileImageView)
//        print(location)
//        print(self.profileImageView.bounds)
////        if !self.profileImageView.bounds.contains(location){
////        }
//
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    struct StoryBoardConstants
    {
        static let backgroundImageName = "bg_login"
        static let requestTag = "CreateAccountScreen"
        static let textFieldBorderErrorColor = UIColor(red: 255/255, green: 154/255, blue: 23/255, alpha: 1).cgColor
        static let textFieldFocusBackgroundColor = UIColor(red: 52/255, green: 100/255, blue: 115/255, alpha: 0.70)
        static let textFieldNormalBackgroundColor = UIColor(red: 52/255, green: 100/255, blue: 115/255, alpha: 0.45)
        static let textFieldBorderWidth = CGFloat(2)
        static let cameraActionButtonTitle = "Camera"
        static let galleryActionButtonTitle = "Gallery"
        static let mediaActionSheetTitle = ""
        static let mediaActionSheetMessage = "Choose Media Source"
        static let cancelActionButtonTitle = "Cancel"
        static let showUserHomePageSegue = "showUserHomePage"
        static let profilePictureBorderWidth = CGFloat(1)
        static let profilePictureBorderColor = UIColor.init(red: 155.0/255.0, green: 223.0/255.0, blue: 70.0/255.0, alpha: 1.0).cgColor
        
        
    }

    func setBackgroundImage(){
        let background_img = UIImageView.init(image: UIImage.init(named: "background_img"))
        background_img.frame = self.view.frame
        self.view.addSubview(background_img)
        self.view.sendSubview(toBack: background_img)
    }
    
    func setStatusBarBackground() {
        let statusBar: UIView = UIApplication.shared.value(forKey:"statusBar") as! UIView
        if statusBar.responds(to: #selector(setter: AVMutableVideoCompositionInstruction.backgroundColor)) {
            statusBar.backgroundColor = AbstractViewController.statusBar_color
        }

    }
    
    /*
     * toastStyle property is the styler of the toast message
     */
    var toastStyle = ToastStyle();

    /*
     * MARK: function that show the message on the screen as a Toast
     */
    func showToastMessage(messag: String)
    {
        DispatchQueue.main.async {
            if(self.tabBarController != nil){
                let y = (self.view.frame.height - self.tabBarController!.tabBar.frame.height) - 50;
                let x = self.view.frame.width/2;
                self.tabBarController!.view.makeToast(messag, duration: 3.0, position: CGPoint(x: x, y: y), style: self.toastStyle)
            }else{
                self.view.makeToast(messag, duration: 3.0, position: .bottom, style: self.toastStyle)
            }
        }
    }

    
    /*
     * MARK: error handling
     */
    func handleError(error: ModelErrors)
    {
        if error == .CameraNotAllowed
        {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Camera Not Allowed", message: ErrorMessages[error]!, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }
            return
        }
        
        if let errorMessage = ErrorMessages[error]
        {
            showToastMessage(messag: errorMessage)
        }
    }
    /*
     *  MARK: public functionS that shows/hides the hud with loading text
     */
    var hudVisibile = false
    var hud : MBProgressHUD?
    
    func showLoading()
    {
        DispatchQueue.main.async
            {
                if self.hudVisibile == false
                {
                    self.hudVisibile = true
                    self.hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                    self.hud?.mode = MBProgressHUDMode.annularDeterminate
                    self.hud?.label.text = "Loading"
                    
                }
        }
        
    }
    
    func hideLoading()
    {
        DispatchQueue.main.async {
            self.hud?.hide(animated: true)
            self.hud = nil
            self.hudVisibile = false
        }
        
    }
    
    func checkConnection() -> Bool{
        if Reachability.init()?.isReachable == false{
            self.showToastMessage(messag: "No connection, please check your connection and try again.")
            return false
        }
        return true
    }

}
