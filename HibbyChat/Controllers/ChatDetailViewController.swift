//
//  ChatDetailViewController.swift
//  HippyChat
//
//  Created by Mostafa on 6/7/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit

class ChatDetailViewController: AbstractViewController {
    
    @IBOutlet weak var chatImageView: UIImageView!
    
    @IBOutlet weak var userImageView: CircularImage!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var messageDateLabel: UILabel!
    @IBOutlet weak var userLocationLabel: UILabel!
    
    var chatMsg : ChatMessage?
    var user : User?
    override func viewDidLoad() {
        self.set_background = false
        super.viewDidLoad()
        if let msg = chatMsg {
            self.chatImageView.sd_setImage(with: URL.init(string: msg.image_url)!)
            self.userNameLabel.text = user!.getDisplayName()
            self.messageDateLabel.text = msg.getDate()
//            self.userLocationLabel.text = user!.getUserCity()
        }
    }
    
    
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
         self.dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
}
