//
//  SearchUserViewController.swift
//  Hippy
//
//  Created by inova5 on 7/17/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit

class SearchUserViewController: AbstractViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, ContactCellDelegate  {
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(SearchUserViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    @IBOutlet var doneButton: RoundedButton!
    
    func openUser(user: User) {
        self.showUserProfile(user)
    }
    var seletectedPhones : NSHashTable<AnyObject> = NSHashTable<AnyObject>.init()
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        // Do some reloading of data and update the table view's data source
        // Fetch more objects from a web service, for example...
        
        // Simply adding an object to the data source for this example
        self.search()
        refreshControl.endRefreshing()
    }

    func didSelectContact(indexPath: IndexPath, selected: Bool) {
        if users.count > indexPath.row {
            let user = users[indexPath.row]
            // TODO
            if !isUserSelected(user: user) {
//                self.seletectedPhones.add(user.phone_number as AnyObject?)
//                self.seletectedUsers.add(user as AnyObject?)
            }else{
//                self.seletectedPhones.remove(user.phone_number as AnyObject?)
//                self.seletectedUsers.remove(user as AnyObject?)
            }
            self.tableView.reloadData()
        }
//        hideDone = false
        self.doneButton.isHidden = false


    }
    var users : [User] = []
    var seletectedUsers : NSHashTable<AnyObject> = NSHashTable<AnyObject>.init()
    var selectUserController : SelectUsersViewController?
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchTextField: TextFieldPlaceHolder!
    override func viewDidLoad() {
        set_background = false
        super.viewDidLoad()
        tableView.addSubview(refreshControl)
    }
    // MARK: Table View Datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactCell
        var user = User()
        if indexPath.section == 1 {
            user = users[indexPath.row]
        }else{
            if users.count > 0 {
                user = users[indexPath.row]
            }else{
                user = users[indexPath.row]
            }
        }
        cell.bindUser(user: user, selected: isUserSelected(user: user), indexPath: indexPath)
        cell.delegate = self
        return cell
    }
    
    func isUserSelected(user : User) -> Bool{
        return true
    }
    
    var hideDone = true
    // MARK: Table View Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // TODO
        self.didSelectContact(indexPath: indexPath, selected: true)
            }

    // MARK: UI Actions
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        goBack(success: false, state: .Other)
    }
    
    func goBack(success:Bool, state: UserChatRoomState){
        _ = self.navigationController?.popViewController(animated: true)
    }
    var page_number = 1
    let page_size = 20
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
       // TODO Search Users Request
        
        if let parentController = self.selectUserController {
//            parentController.seletectedPhones = self.seletectedPhones.copy() as! NSHashTable<AnyObject>
            
            if let _users = self.seletectedUsers.allObjects as? [User]{
                for user in _users {
                    var exist = false
//                    for tUser in parentController.suggestedUsers {
//                        if user.id == tUser.id {
//                            exist = true
//                            break
//                        }
//                    }
                    if !exist {
//                        parentController.suggestedUsers.append(user)
                    }
                }
            }
//            parentController.selectedCount = parentController.seletectedPhones.allObjects.count
            if parentController.selectedCount >= 2 {
                parentController.submitButton.isHidden = false
                // Show Button
            }else{
                parentController.submitButton.isHidden = true
                // Hide Button
            }
            
//            parentController.filterContacts()
        }

        goBack(success: false, state: .Other)
      
    }
    func search(){
        if self.checkConnection(){
            self.showLoading()
            UserManager().searchUsers(username: self.searchTextField.text!, page_number: page_number, page_size: page_size) { (result) in
                self.hideLoading()
                if result.status {
                    if let searchResult = result.results as? [User]{
                        if self.page_number == 1 {
                            self.users = searchResult
                        }else{
                        
                            self.users.append(contentsOf: searchResult)
                        }
                    }else{
                        self.users = []
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print(indexPath.row)
        if (indexPath.row + 5) >= (page_number * page_size){
            
            page_number = page_number + 1
            print("Next Page \(page_number)")
            search()
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        page_number = 1
        search()
        textField.resignFirstResponder()
        return true
    }
}
