//
//  UserViewController.swift
//  HibbyChat
//
//  Created by Jihad Ismail on 10/11/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import UIKit

class UserViewController: AbstractViewController, UITextFieldDelegate {

    @IBOutlet weak var userIdTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if self.userIdTextField != nil {
        let userId = Int(self.userIdTextField.text!)
        self.view.endEditing(true)

        var user = User()
        user.name = "default name"
        switch userId! {
        case 5:
            user.id = 5
            user.access_token = "ZGBLxnpDWcwCZcfrINecsTXsoNMxOIOVNOWdqTVfzyKhVVGFie"

            UserManager().cacheCurrentUser(user: user)
            return true

            
        case 6:
            user.id = 6
            user.access_token = "MZoyYXTGrAwsFUcusFYcDoPaKWyTjStnYrXSlLObDvMcQjwECN"
            UserManager().cacheCurrentUser(user: user)
            return true

            
        case 7:
            user.id = 7
            user.access_token = "LEgbfDuTYeGZIELHQmeIwJGfywjlsylTrjoyZZoieGJuhufjIb"

            UserManager().cacheCurrentUser(user: user)
            return true

            
        case 8:
            user.id = 8
            user.access_token = "bHaEvGEJgSzutRycKqVStaWJipfJHDEFdKzfeFTagzeNvDcbFd"
            UserManager().cacheCurrentUser(user: user)
            return true

            
        case 9:
            user.id = 9
            user.access_token = "efvakLypbvTGJMJBLGXwKRHrdhkuTyEDHvavMaRBycBdzoyJUB"
            UserManager().cacheCurrentUser(user: user)
            return true

            
        default:
            self.showToastMessage(messag: "Sorry you must choose id from 5 : 9 only.")
            return true

            
        }
        }
        return false
    }
    
    @IBAction func GoAction(_ sender: UIButton) {
        if self.userIdTextField != nil{
        let userId = Int(self.userIdTextField.text!)
        switch userId! {
        case 5 ... 9:
            performSegue(withIdentifier: "userChatRooms", sender: nil)
            break
        default:
             self.showToastMessage(messag: "Sorry you must choose id from 5 : 9 only.")
            break
        }
        }
    }

}
