//
//  SelectUsersViewController.swift
//  Hippy
//
//  Created by Mostafa on 5/16/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit
import Contacts

enum UserChatRoomState {
    case Sent
    case Other
}



class SelectUsersViewController: AbstractViewController,UITableViewDelegate,UITableViewDataSource,ContactCellDelegate,UITextFieldDelegate, UsersHeaderDelegate {
    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet var searchTextField: TextFieldPlaceHolder!
    @IBOutlet weak var hintView: UIView!
    @IBOutlet weak var tableView: UITableView!

    var chatRoomImageURL = ""
    var delegate : SelectUsersDelegate?
    var groupType : ChatRoomType = .Group
    var selectedCount = 0
    var allUsers: [User] = []
    var selectedUsers: [User] = []
    var filteredContacts: [User] = []
    
    var chatRoom_title = ""
    
    enum ChatRoomType {
        case Group
    }
    
    var contacts: [NSDictionary] = []
    
    override func viewDidLoad() {
        
        set_background = false
        searchTextField.delegate = self
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tableView.register(UINib.init(nibName: "UserListHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "headerView")
        self.checkContacts()
        super.viewDidLoad()
    }
    
    
    func checkContacts(){
        if self.checkConnection(){
            self.showLoading()
            UserManager().getUsers(pageNumber: 1, pageSize: 50, handler: { (response) in
            
                if response.status {
                    self.hideLoading()

                    if let users = response.results as? [User]{
                    
                        for user in users {
                            if(user.id != UserManager().loadCachedUser()?.id){
                                user.name = "\(user.id)"
                                self.contacts.append(["user" : user, "selected": false])
                                self.allUsers.append(user)
                            }
                        }
                        self.tableView.reloadData()
                    }else if let error = response.error {
                        self.showToastMessage(messag: error.domain)
                    }
                }else{
                    if let error = response.error{
                        self.showToastMessage(messag: error.domain)
                    }else{
                        self.showToastMessage(messag: "Failed To get users")
                    }
                }
            
            })
        }
    }
    
    func openUser(user: User) {
        self.showUserProfile(user)
    }
    
    
    // MARK: UsersHeaderDelegate Methods
    func addPlayers() {
        self.performSegue(withIdentifier: "searchUsers", sender: nil)
    }
    
    func inviteFriends() {
        shareApp()
    }
    
    
    func textFieldDidChange(_ textField: UITextField) {
        self.filter(key: textField.text!)
    }
    

    func filter(key: String) {
        let searchKey = key.capitalized
        if searchKey.characters.count == 0 {
            self.tableView.reloadData()
            return
        }
        
        for user in self.allUsers {
            if user.getDisplayName().capitalized.contains(searchKey){
                self.filteredContacts.append(user)
            }
        }
        self.tableView.reloadData()
    }
    
    //MARK: Contact Cell Delegate
    func didSelectContact(indexPath: IndexPath, selected: Bool) {
        //TODO

        let user = self.contacts[indexPath.row]["user"] as! User
        if selected {
            
            selectedCount = selectedCount + 1
            self.selectedUsers.append(user)
            self.contacts[indexPath.row] = ["user" : user,"selected": true]
        }else{
            
            selectedCount = selectedCount - 1
            self.selectedUsers.remove(at: indexPath.row)
            self.contacts[indexPath.row] = ["user" : user,"selected": false]
        }
        
        if selectedCount >= 2 {
            submitButton.isHidden = false
        }else{
            submitButton.isHidden = true
        }
    }
    
    
    //MARK: Table View Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
            return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contacts.count
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = self.view.backgroundColor
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactCell
        
        cell.bindUser(user: self.contacts[indexPath.row]["user"] as! User, selected: self.contacts[indexPath.row]["selected"] as! Bool, indexPath: indexPath)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let type = 1
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerView")
        
        if let header = view as? HeaderCell {
            header.bind(type: type)
            header.delegate = self
        }
        
        return view
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "Other Users"
    }
    
    @IBAction func hideHintView(_ sender: UIButton) {
        self.hintView.removeFromSuperview()
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        goBack(success: false)
    }
    
    func goBack(success:Bool){
        self.delegate?.didFinish(success: success)
       self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        let chatRoom = ChatRoom()
        chatRoom.title = chatRoom_title
        
        chatRoom.image_url = self.chatRoomImageURL
        chatRoom.users = []
        
        let currentUser = UserManager().loadCachedUser()!
        
        chatRoom.users.append(currentUser)
        
        for user in self.selectedUsers{
           chatRoom.users.append(user)
        }
        
        if chatRoom.users.count >= 2, chatRoom.users.count <= 20 {
            // Valid count
        }else{
            showToastMessage(messag: "Group should contain at least 2 users & at most 20")
            return
        }
        
        if self.checkConnection(){
            self.showLoading()
        
            chatRoom.title = chatRoom_title

            ChatRoomManager().createChatRoom(chatRoom: chatRoom) { (result) in
                self.hideLoading()
                
                if result.status {
            
                    self.showToastMessage(messag: "Group created successfully")
                    self.goBack(success: true)
                }else{
                    if let error = result.error{
                        self.showToastMessage(messag: error.domain)
                    }else{
                        self.showToastMessage(messag: "Failed To Send Hibby")
                    }
                }
            }
        }
        print("Submit Button Pressed")
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? SearchUserViewController {
            dest.selectUserController = self
        }
    }
}
