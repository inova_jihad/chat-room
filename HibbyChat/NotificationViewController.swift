//
//  NotificationViewController.swift
//  Hippy
//
//  Created by inova5 on 7/13/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit

class NotificationViewController: UIViewController {
    
    @IBOutlet var hibbyTitleTextField: TextFieldPlaceHolder!
    
    var parentController : AbstractViewController?
    var notification_body : String = ""
    var group_id : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.isOpaque = false
        view.backgroundColor = UIColor.clear
        self.hibbyTitleTextField.text = notification_body
    }
    
    @IBAction func GoButtonPressed(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
//        self.parentController?.openGroup(groupID: group_id)
    }
    
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var imageView: UIImageView!
    
    @IBAction func didTapOnView(_ sender: UITapGestureRecognizer) {
        let location = sender.location(in: self.imageView)
        print(location)
        print(self.imageView.bounds)
        if !self.imageView.bounds.contains(location){
            
            self.dismiss(animated: true, completion: { 
                
            })
        }
    }
    
}
