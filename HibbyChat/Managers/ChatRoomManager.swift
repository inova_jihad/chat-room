//
//  GroupManager.swift
//  Hippy
//
//  Created by Mostafa on 5/16/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import ReachabilitySwift
import Alamofire

class ChatRoomManager: AbstractManager {
    
    func chatMsgSent(chatMessage: ChatMessage, handler: @escaping (APIResponse) -> Void){
        // TODO Get user groups
        
        let url = URL.init(string: Constants.SEND_NEW_MESSAGE_URL)
        let headers = ["Authorization" : "Token token=\(UserManager().readUserToken())"]
        
        let params : [String : Any] = ["chat_room_message" : chatMessage.toDictionary()]
        
        Alamofire.request(url!, method: .post, parameters: params, headers: headers).responseJSON
            {response in
                let meta = self.getStatus(response: response)
                if !meta.success {
                    // Error
                    let apiResponse : APIResponse = APIResponse.init()
                    apiResponse.status = false
                    apiResponse.error = NSError.init(domain: meta.error_message, code: 404, userInfo: nil)
                    handler(apiResponse)
                    return
                }else{
                    // Parse User
                    switch response.result {
                    case .success( _):
                        let apiResponse = APIResponse.init()
                        apiResponse.status = true
                        handler(apiResponse)
                        break;
                    default:
                        let apiResponse = APIResponse.init()
                        apiResponse.status = false
                        handler(apiResponse)
                        
                        break;
                    }
                }
        }
    }
    
    func visitChatRoom(chatRoom: ChatRoom, handler: @escaping (APIResponse) -> Void){
        // TODO Get user groups
        
        let url = URL.init(string: Constants.VISIT_CHAT_ROOM_URL)
        let user = UserManager().loadCachedUser()
        let headers = ["Authorization" : "Token token=\(user?.access_token)"]
        
        let params : [String : Any] = ["user_id" : user?.id ?? -1, "chat_room_id": chatRoom.id]
        
        Alamofire.request(url!, method: .post, parameters: params, headers: headers).responseJSON
            {response in
                let meta = self.getStatus(response: response)
                if !meta.success {
                    // Error
                    let apiResponse : APIResponse = APIResponse.init()
                    apiResponse.status = false
                    apiResponse.error = NSError.init(domain: meta.error_message, code: 404, userInfo: nil)
                    handler(apiResponse)
                    return
                }else{
                    // Parse User
                    switch response.result {
                    case .success( _):
                        let apiResponse = APIResponse.init()
                        apiResponse.status = true
                        handler(apiResponse)
                        break;
                    default:
                        let apiResponse = APIResponse.init()
                        apiResponse.status = false
                        handler(apiResponse)
                        
                        break;
                    }
                }
        }
    }
    
    func createChatRoom(chatRoom: ChatRoom, handler: @escaping (APIResponse) -> Void){
        // TODO Get user groups
        
        let url = URL.init(string: "\(Constants.CREATE_CHAT_ROOM_URL)")
        let headers = ["Authorization" : "Token token=\(UserManager().loadCachedUser()?.access_token)"]
        
        let params = chatRoom.convertToHTTPParameters()
        
        Alamofire.request(url!, method: .post, parameters: params, headers: headers).responseJSON
            {response in
                let meta = self.getStatus(response: response)
                
                if !meta.success {
                    // Error
                    let apiResponse : APIResponse = APIResponse.init()
                    apiResponse.status = false
                    apiResponse.error = NSError.init(domain: meta.error_message, code: 404, userInfo: nil)
                    handler(apiResponse)
                    return
                }else{
                    // Parse User
                    switch response.result {
                    case .success(let value):
                        if let dict = value as? NSDictionary {
                            if let chatRoomDict = dict.value(forKey: "chat_room") as? NSDictionary {
                                let chatRoom = ChatRoom()
                                if chatRoom.bindDictionary(dict: chatRoomDict) {
                                    let apiResponse = APIResponse.init()
                                    apiResponse.result =
                                    ["chat_room" : chatRoom ] as NSDictionary
                                    
                                    apiResponse.status = true
                                    handler(apiResponse)
                                    return
                                }
                            }
                        }
                        let apiResponse = APIResponse.init()
                        apiResponse.status = false
                        handler(apiResponse)
                        break;
                    default:
                        break;
                    }
                }
        }
    }
    
    func getUserChatRooms(user: User, handler: @escaping (APIResponse) -> Void){
        // TODO Get user groups
        
        let url = URL.init(string: Constants.USER_CHAT_ROOMS_URL)
        
        let headers = ["Authorization" : "Token token=\(user.access_token)"]
        
        let parameters = ["user_id": user.id, "page_number": 1 ,"page_size": 100] as [String : Any]
        
        Alamofire.request(url!, method: .get, parameters: parameters, headers: headers).responseJSON
            {response in
                let meta = self.getStatus(response: response)
                if !meta.success {
                    // Error
                    let apiResponse : APIResponse = APIResponse.init()
                    apiResponse.status = false
                    apiResponse.error = NSError.init(domain: meta.error_message, code: 404, userInfo: nil)
                    handler(apiResponse)
                    return
                }else{
                    // Parse User
                    switch response.result {
                    case .success(let value):
                        var chatRooms : [ChatRoom] = []
                        if let dict = value as? NSDictionary {
                            if let chatRoomsArray = dict.value(forKey: "chatRooms") as? [NSDictionary] {
                                for chatRoomDict in chatRoomsArray {
                                    let chatRoom = ChatRoom()
                                    if chatRoom.bindDictionary(dict: chatRoomDict) {
                                        chatRooms.append(chatRoom)
                                    }
                                }
                            }
                        }
                        let apiResponse = APIResponse.init()
                        apiResponse.results = chatRooms
                        apiResponse.status = true
                        handler(apiResponse)
                        break;
                    default:
                        break;
                    }
                }
        }

    }
}
