//
//  UserManager.swift
//  Hippy
//
//  Created by Mostafa on 5/14/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import Alamofire
import ReachabilitySwift
import CoreLocation

class UserManager :AbstractManager {

    func registerDeviceToken(deviceToken:String) {
        if loadCachedUser() != nil {

            let url = URL.init(string: Constants.REGISTER_USER_DEVICE_URL)

            let headers = ["Authorization" : "Token token=\(readUserToken())"]
            
            
            var uniqueId = "temp_id"
            if let id = UIDevice.current.identifierForVendor?.uuidString {
                uniqueId = id
            }

            let params = [
                "fcm_token" : UserManager().readNotifToken(),
                "device_token": uniqueId
            ]
            Alamofire.request(url!, method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON{ response in
                print(response)
            }

        }
        

    }
    

    
    func getUsers(pageNumber: Int, pageSize: Int, handler: @escaping (APIResponse) -> Void) -> Void{
        
        let url = URL.init(string: Constants.GET_USERS_URL)
        
        let params = ["page_number": pageNumber , "page_size": pageSize ]
        
        Alamofire.request(url!, method: .get, parameters: params, headers: nil).responseJSON
            {response in
                let meta = self.getStatus(response: response)
                if !meta.success {
                    // Error
                    let apiResponse : APIResponse = APIResponse.init()
                    apiResponse.status = false
                    apiResponse.error = NSError.init(domain: meta.error_message, code: 404, userInfo: nil)
                    handler(apiResponse)
                    return
                }else{
                    // Parse User
                    switch response.result {
                        case .success(let value):
                            if let dict = value as? NSDictionary {
                                if let usersArray = dict.value(forKey: "users") as? [NSDictionary] {
                                    let users : [User] = User.getList(list: usersArray as NSArray)
                                    let apiResponse : APIResponse = APIResponse.init()
                                    apiResponse.results = users
                                    apiResponse.status = true
                                    handler(apiResponse)
                                    return
                                }
                            }
                        break;
                        default:
                        break;
                    }
                    meta.parseMeta(dictionary: [:])
                    let apiResponse : APIResponse = APIResponse.init()
                    apiResponse.status = false
                    apiResponse.error = NSError.init(domain: meta.error_message, code: 404, userInfo: nil)
                    handler(apiResponse)
                }
            }
        }
    
    func searchUsers(username: String, page_number : Int, page_size: Int, handler: @escaping (APIResponse) -> Void) -> Void {
        
        let url = URL.init(string: Constants.SEARCH_USERS_URL)
        let headers = ["Authorization" : "Token token=\(UserManager().readUserToken())"]
        let params : [String : Any] = ["search_key" : username, "page_number" : page_number, "page_size" : page_size]
        
        Alamofire.request(url!, method: .post, parameters: params, headers: headers).responseJSON
            {response in
                let meta = self.getStatus(response: response)
                if !meta.success {
                    // Error
                    let apiResponse : APIResponse = APIResponse.init()
                    apiResponse.status = false
                    apiResponse.error = NSError.init(domain: meta.error_message, code: 404, userInfo: nil)
                    handler(apiResponse)
                    return
                }else{
                    // Parse User
                    switch response.result {
                    case .success(let value):
                        if let dict = value as? NSDictionary {
                            if let usersArray = dict.value(forKey: "users") as? [NSDictionary] {
                                let users : [User] = User.getList(list: usersArray as NSArray)
                                let apiResponse : APIResponse = APIResponse.init()
                                apiResponse.results = users
                                apiResponse.status = true
                                handler(apiResponse)
                                return
                            }
                        }
                        break;
                    default:
                        break;
                    }
                    meta.parseMeta(dictionary: [:])
                    let apiResponse : APIResponse = APIResponse.init()
                    apiResponse.status = false
                    apiResponse.error = NSError.init(domain: meta.error_message, code: 404, userInfo: nil)
                    handler(apiResponse)
                    
                }
        }
    }
    
    func getUserTokenWrapper() -> KeychainItemWrapper{
        let keyChain:KeychainItemWrapper = KeychainItemWrapper(identifier: "user_token", accessGroup: nil)
        return keyChain
    }
    
    func getNotifTokenWrapper() -> KeychainItemWrapper{
        let keyChain:KeychainItemWrapper = KeychainItemWrapper(identifier: "fcm_token", accessGroup: nil)
        return keyChain
    }
    
    func storeUserToken(token:String) -> Void {
        let wrapper:KeychainItemWrapper = self.getUserTokenWrapper()
        wrapper.setObject("user_token", forKey: kSecAttrAccount)
        wrapper.setObject(token, forKey: kSecValueData)
        wrapper.setObject(kSecAttrAccessibleAlways, forKey:kSecAttrAccessible)
    }
    
    func readUserToken() -> String {
        let wrapper:KeychainItemWrapper = self.getUserTokenWrapper()
        let token:String = wrapper.object(forKey: kSecValueData) as! String
        return token
    }
    
    func storeNotifToken(token:String) -> Void {
        let wrapper:KeychainItemWrapper = self.getNotifTokenWrapper()
        wrapper.setObject("fcm_token", forKey: kSecAttrAccount)
        wrapper.setObject(token, forKey: kSecValueData)
        wrapper.setObject(kSecAttrAccessibleAlways, forKey:kSecAttrAccessible)
    }
    
    func readNotifToken() -> String {
        let wrapper:KeychainItemWrapper = self.getNotifTokenWrapper()
        let token:String = wrapper.object(forKey: kSecValueData) as! String
        return token
    }
    
    func clearUserToken() -> Void {
        let wrapper:KeychainItemWrapper = self.getUserTokenWrapper()
        wrapper.resetKeychainItem();
    }
    
    func clearCurrentUser(){
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: "current_user")
        userDefaults.synchronize()
    }
    
    func cacheCurrentUser(user: User){
        let userDefaults = UserDefaults.standard
        userDefaults.set(NSKeyedArchiver.archivedData(withRootObject: user), forKey: "current_user")
        userDefaults.synchronize()
    }
    
    func loadCachedUser() -> User? {
        
        let userDefaults = UserDefaults.standard
        if let _ = userDefaults.object(forKey: "current_user") {
            let decodedNSData = userDefaults.object(forKey: "current_user") as! Data
            let currentUser = NSKeyedUnarchiver.unarchiveObject(with: decodedNSData) as! User

            return currentUser
        }
        
        return nil
    }
    
}
