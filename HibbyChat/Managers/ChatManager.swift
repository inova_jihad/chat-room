//
//  ChatManager.swift
//  HippyChat
//
//  Created by Mostafa on 5/18/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import Alamofire
import FirebaseDatabase
import ReachabilitySwift


class ChatManager: AbstractManager {
    
    var query : DatabaseQuery?
    
    func sendChat(chatMessage : ChatMessage, chatRoom: ChatRoom, handler: @escaping (APIResponse?) -> Void){
        
        let response = APIResponse()
        
        chatMessage.chat_room_id = chatRoom.id
        
        if let id = chatMessage.insert(underPath: chatRoom.getChatPath()) {
            chatMessage.fir_Id = id
            chatMessage.FIR_path = chatRoom.getChatPath() + "/\(id)"

            response.status = true
        }else{
            response.status = false
        }
        
        handler(response)

    }
    
    func getChatMessages(chatRoom: ChatRoom, handler: @escaping (APIResponse?) -> Void){
        
        let ref = Database.database().reference(withPath: chatRoom.getChatPath())
        self.query?.removeAllObservers()
        self.query = ref.queryOrderedByKey()
        self.query!.observe(.value) { (snapShot, key) in
            let apiResponse = APIResponse()
            apiResponse.status = true
            var chatMsgs : [ChatMessage] = []
            
            for child in snapShot.children {
                if let element = child as? DataSnapshot {
                    if let dict = element.value as? NSDictionary{
                        dict.setValue(element.key, forKey: "id")
                        let chatMsg = ChatMessage()
                        chatMsg.fir_Id = dict.value(forKey: "id") as! String
                        chatMsg.date = dict.value(forKey: "date") as! String
                        chatMsg.message = dict.value(forKey: "message") as! String
                        chatMsg.type = dict.value(forKey: "type") as! Int
                        chatMsg.chat_room_id = chatRoom.id
                        chatMsg.FIR_path = chatRoom.getChatPath() + "/\(chatMsg.fir_Id)"
                        if let id = dict.value(forKey: "user_id") as? Int {
                            chatMsg.user_id = id
                        }else{
                            chatMsg.user_id = 1
                        }
                        if let imageURL = dict.value(forKey: "image_url") as? String {
                            chatMsg.image_url = imageURL
                        }else{
                            chatMsg.image_url = ""
                        }
                        chatMsgs.append(chatMsg)
                        }
                    }
            }
            apiResponse.results = chatMsgs
            handler(apiResponse)
        }
    }
    
}
