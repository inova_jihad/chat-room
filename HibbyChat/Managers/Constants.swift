//
//  Constants.swift
//  Hippy
//
//  Created by Mostafa on 5/14/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit

class Constants{
    static let FIREBASE_URL = "https://demoproject-c3f40.firebaseio.com/"
    static let BASE_URL = "https://frozen-chamber-51571.herokuapp.com/"
    static let API_URL = "\(BASE_URL)api/"
    static let VERSION_URL = "\(API_URL)v1/"
    
    static let GET_USERS_URL = "\(BASE_URL)users"
    static let USER_CHAT_ROOMS_URL = "\(VERSION_URL)chat_rooms_user"
    static let SEARCH_USERS_URL = "\(VERSION_URL)users/search_users"
    static let CREATE_CHAT_ROOM_URL = "\(VERSION_URL)chat_room"
    static let VISIT_CHAT_ROOM_URL = "\(VERSION_URL)visit_chat_room"
    static let SEND_NEW_MESSAGE_URL = "\(VERSION_URL)new_message"
    static let REGISTER_USER_DEVICE_URL = "\(VERSION_URL)users/reg_device"
    static let GET_CHAT_ROOM_MESSAGES_URL = "/chatMessages"
    static var navController : UINavigationController?
    static var currentView : AbstractViewController?
    static var chatRoomsView: ChatRoomsViewController?
}
