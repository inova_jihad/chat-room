//
//  Chat.swift
//  HippyChat
//
//  Created by Mostafa on 5/18/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import Firebase

class ChatMessage: BaseModel {
    
    var message : String = ""
    var date : String = ""
    var fir_Id : String = ""
    var FIR_path: String = ""
    var user_id : Int = 0
    var image_url : String = ""
    var type : Int = 1
    var chat_room_id: Int = 0
    
    override func bindDictionary(dict: NSDictionary) -> Bool {
        if !super.bindDictionary(dict: dict){
            return false
        }
        
        if let firId = dict.object(forKey: "fir_id") as? String{
            self.fir_Id = firId
        }
        
        if let firPath = dict.object(forKey: "FIR_path") as? String{
            self.FIR_path = firPath
        }
        
        if let chatRoomId = dict.object(forKey: "chat_room_id") as? Int{
            self.chat_room_id = chatRoomId
        }
        
        if let userId = dict.object(forKey: "user_id") as? Int{
            self.user_id = userId
        }
        
        if let image = dict.object(forKey: "image_url") as? String{
            self.image_url = image
        }
        
        if let date = dict.object(forKey: "created_at") as? String{
            self.date = date
        }
        
        return true
    }

    
    func insert(underPath path: String) -> String? {
        let dict = toDictionary()
        guard dict.keys.count > 0 else { return nil }
        let ref = Database.database().reference().child( path ).childByAutoId()
        NSLog("%@", dict.description)
        ref.setValue(dict) { (error, refrence) in
            if let err = error {
                NSLog("%@", err.localizedDescription)
            }else{
                NSLog("%@", refrence.key)
            }
        }
        return ""
    }
    
    func toDictionary() -> [String:Any] {
        var dict = [String:Any]()
        Mirror(reflecting: self).children.forEach { dict[$0.label!] = $0.value }
        dict.removeValue(forKey: "id")
        return dict
    }
    
    func getDate() -> String {
        let date = self.date.parseFirebaseDate()
        return date.getDateRep()
    }
}
