//
//  Meta.swift
//  Hippy
//
//  Created by Mostafa on 5/14/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import Alamofire

class Meta{
    
    var status:String = "200";
    var message:String = "";
    var error_message: String = "";
    var success = true
    
    static let invalid_response = "Failed with no status code"
    
    func parseMeta(dictionary: NSDictionary){
        if let metaDict = dictionary.value(forKey: "meta") as? NSDictionary {
            if let status = metaDict.value(forKey: "status") as? Int {
                self.status = "\(status)"
                if let message = metaDict.value(forKey: "message") as? String {
                    self.message = message
                    if self.status != "200" {
                        self.checkErrors(dict: dictionary)
                        self.success = false
                        return
                    }else{
                        return
                    }
                }
            }
        }
        self.setInvalidMessage()
        return
    }
    
    func checkErrors(dict: NSDictionary){
        if let error = dict.object(forKey: "errors") as? String {
            self.error_message = error
        }else{
            self.error_message = self.message
        }
    }
    
    func setInvalidMessage(){
        self.status = "401"
        self.error_message = Meta.invalid_response
        self.success = false
    }
}
