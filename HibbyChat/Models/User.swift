//
//  User.swift
//  Hippy
//
//  Created by Mostafa on 5/14/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit

class User: BaseModel, NSCoding {

    var name : String = ""
    var image_url : String = ""
    var access_token : String = ""
    
    override func bindDictionary(dict: NSDictionary) -> Bool {
        if !(super.bindDictionary(dict: dict)){
            return false
        }
        
        if let name = dict.value(forKey: "name") as? String {
            self.name = name
        }else{
            self.name = "default name"
//            return false
        }
        
        if let url = dict.value(forKey: "image_url") as? String {
            self.image_url = url
        }
        
        if let token = dict.value(forKey: "access_token") as? String {
            self.access_token = token
        }
        
        return true
    }
    
    
    // MARK: NSCoding
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encodeCInt(Int32(id), forKey: "id")
        aCoder.encode(image_url, forKey:"image_url")
        aCoder.encode(access_token, forKey:"access_token")

    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        // Mandatory attributes
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let id = aDecoder.decodeInt32(forKey: "id")
        let image_url = aDecoder.decodeObject(forKey: "image_url") as? String
        let access_token = aDecoder.decodeObject(forKey: "access_token") as? String
        self.init()
        self.name = name
        self.id = Int(id)
        if let accessToken = access_token{
            self.access_token = accessToken
        }
        if let _img = image_url {
            self.image_url = _img
        }
    }
    
    func convertToHTTPParameters() -> [String:Any] {
        
        let httpParameters : [String:Any] = ["name": name,  "user[image_url]":image_url]
        return httpParameters
        
    }
    static func getList(list : NSArray) -> [User] {
        var result : [User] = []
        for stateDict in list {
            if let dict = stateDict as? NSDictionary{
                let user = User()
                if (user.bindDictionary(dict: dict))
                {
                    result.append(user)
                }
            }
        }
        return result
    }

    func getDisplayName()-> String{

        return self.name

    }
    
    func getPlaceholderImage()-> UIImage {
        
        return #imageLiteral(resourceName: "placeholder_male")
    }

}
