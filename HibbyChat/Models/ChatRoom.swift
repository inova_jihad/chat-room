//
//  ChatRoom.swift
//  HippyChat
//
//  Created by Mostafa on 5/16/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation

class ChatRoom: BaseModel {
    
    var owner_id : Int = 0
    var title : String = ""
    var users : [User] = []
    var image_url : String = ""
    var date : String = ""
    var unreadCount : Int = 0
    var chatPath: String = ""
    
    override func bindDictionary(dict: NSDictionary) -> Bool {
        if !super.bindDictionary(dict: dict){
            return false
        }

        if let title = dict.object(forKey: "title") as? String{
            self.title = title
        }
        
        if let chatPath = dict.object(forKey: "chat_path") as? String{
            self.chatPath = chatPath
        }
        
        if let date = dict.object(forKey: "created_at") as? String{
            self.date = date
        }

        if let usersDict = dict.object(forKey: "users") as? [NSDictionary] {
            self.users = User.getList(list: usersDict as NSArray)
        }
        
        if let unreadCount = dict.object(forKey: "unread_count") as? Int {
            self.unreadCount = unreadCount
        }
        
        if let url = dict.object(forKey: "group_image") as? String{
            self.image_url = url
        }else{
            self.image_url = ""
        }
        
        return true
    }
    
    func convertToHTTPParameters() -> [String:Any] {
        
        var httpParameters : [String : Any] = ["chat_title" : self.title]
        var index = 1
        
        for user in users {
            
            httpParameters["users_ids[\(index)]"] = user.id
            index += 1
        }
        
        return httpParameters
    }
    
    func getChatPath() -> String{
        return self.chatPath + Constants.GET_CHAT_ROOM_MESSAGES_URL
    }
    
    func getDate() -> String {
        let date = self.date.parseServerDate()
        print (date.description)
        print(date.getDateRep())
        return date.getDateRep()
    }
}
