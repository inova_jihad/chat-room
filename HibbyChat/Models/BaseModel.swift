//
//  BaseModel.swift
//  HippyChat
//
//  Created by Mostafa on 5/14/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation

class BaseModel : NSObject{
    
    var id : Int = -1
    
    func bindDictionary(dict: NSDictionary) -> Bool {
        if let _id = dict.value(forKey: "id") as? Int
        {
            self.id = _id
            return true
        }
        return false
    }
}
