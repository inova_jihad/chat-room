//
//  ModelErrors.swift
//  Hippy
//
//  Created by Mostafa on 5/15/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation

enum ModelErrors: Int, Error
{
    case NoInternetConnection
    case NoDataResults
    case MissingMandatoryAttributes
    case NotImplementedYet  // this error is used only in the development
    case InvalidAttributes
    case InvalidPassword
    case UserNotFound
    case ServerError
    case CameraNotAllowed
    case MediaNotAllowed
    case InvalidAppointment
    case RateCouldntBeAdded
    case NoSuckBarberWithThisId
    case AppointmentIsNotInAcceptedState
    case NoSuchAppointmentWithThisId
    case CouldntUpdateThisAppointment
}

let ErrorMessages = [
    ModelErrors.UserNotFound : NSLocalizedString("ModelErrors.UserNotFound", comment: ""),
    ModelErrors.InvalidAttributes : NSLocalizedString("ModelErrors.InvalidAttributes", comment: ""),
    ModelErrors.InvalidAppointment : NSLocalizedString("ModelErrors.InvalidAppointment", comment: ""),
    ModelErrors.InvalidPassword : NSLocalizedString("ModelErrors.InvalidPassword", comment: ""),
    ModelErrors.CameraNotAllowed : NSLocalizedString("ModelErrors.CameraNotAllowed", comment: ""),
    ModelErrors.RateCouldntBeAdded : NSLocalizedString("ModelErrors.RateCouldntBeAdded", comment: ""),
    ModelErrors.NoSuckBarberWithThisId : NSLocalizedString("ModelErrors.NoSuckBarberWithThisId", comment: ""),
    ModelErrors.AppointmentIsNotInAcceptedState : NSLocalizedString("ModelErrors.AppointmentIsNotInAcceptedState", comment: ""),
    ModelErrors.NoSuchAppointmentWithThisId : NSLocalizedString("ModelErrors.NoSuchAppointmentWithThisId", comment: ""),
    ModelErrors.CouldntUpdateThisAppointment : NSLocalizedString("ModelErrors.CouldntUpdateThisAppointment", comment: ""),
    ModelErrors.ServerError : NSLocalizedString("ModelErrors.ServerError", comment: ""),
    ModelErrors.NoInternetConnection : NSLocalizedString("ModelErrors.NoInternetConnection", comment: "")
]
