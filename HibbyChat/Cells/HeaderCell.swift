//
//  HeaderCell.swift
//  Hippy
//
//  Created by inova5 on 7/17/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit


class HeaderCell: UITableViewHeaderFooterView {
    
    var delegate : UsersHeaderDelegate?
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var actionButton: RoundedButton!
    
    func bind(type: Int){
            self.titleLabel.text = "Other Users"
            self.actionButton.setTitle("Invite Friends", for: .normal)
    }
    
    @IBAction func takeAction(_ sender: UIButton) {
            self.delegate?.inviteFriends()
    }
}
