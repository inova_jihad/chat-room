//
//  ChatMessageCell.swift
//  HippyChat
//
//  Created by Mostafa on 5/22/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit


class ChatMessageCell: AbstractChatMessageCell {
    
    @IBOutlet weak var chatImage: UIImageView!
    
    override func bindMsg(msg: ChatMessage, user: User) {
        if let _ = self.chatImage {
        var chatImageFrame = self.chatImage.frame

        if msg.image_url != "" {
            self.chatImage.sd_setImage(with: URL.init(string: msg.image_url)!, placeholderImage: #imageLiteral(resourceName: "imgPlaceholder"))
            chatImageFrame.size.height = 129.5
            self.chatImage.isHidden = false
        }else{
            chatImageFrame.size.height = 0
            self.chatImage.isHidden = true
        }
            self.chatImage.frame = chatImageFrame
            self.chatImage.superview?.layoutIfNeeded()
        }
        super.bindMsg(msg: msg, user: user)
        
    }
    
}
