//
//  ChatMessageSystem.swift
//  HippyChat
//
//  Created by Mostafa on 5/22/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit

class ChatMessageSystemCell: AbstractChatMessageCell {
        @IBOutlet weak var systemImage: UIImageView!
    override func bindMsg(msg: ChatMessage, user: User) {
        super.bindMsg(msg: msg, user: user)
        switch msg.type {
        case 2:
            self.systemImage.image = #imageLiteral(resourceName: "imgHibbySent")
            self.messageHolderView.backgroundColor = UIColor.white
            self.sentArrowImage.image = #imageLiteral(resourceName: "sayWhite")
        case 3:
            self.systemImage.image = #imageLiteral(resourceName: "imgWinner")
            self.messageHolderView.backgroundColor = UIColor.init(red: 255.0/255.0, green: 246.0/255.0, blue: 212.0/255.0, alpha: 1.0)
            self.sentArrowImage.image = #imageLiteral(resourceName: "sayYellow")
        case 5:
            self.systemImage.image = #imageLiteral(resourceName: "imgHibbySent")
            self.messageHolderView.backgroundColor = UIColor.white
            self.sentArrowImage.image = #imageLiteral(resourceName: "sayWhite")
        default:
            self.systemImage.image = #imageLiteral(resourceName: "imgLosser")
            self.messageHolderView.backgroundColor = UIColor.init(red: 255.0/255.0, green: 213.0/255.0, blue: 213.0/255.0, alpha: 1.0)
            self.sentArrowImage.image = #imageLiteral(resourceName: "sayRed")
        }
    }
    
}
