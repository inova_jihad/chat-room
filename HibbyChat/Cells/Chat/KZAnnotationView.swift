//
//  KZNAnnotationView.swift
//  Kazyon
//
//  Created by inova5 on 5/18/16.
//  Copyright © 2016 Inova. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class KZNAnnotationView : MKAnnotationView
{
    var userImageView : CircularImage?
    let selectedLabel:UILabel = UILabel.init(frame:CGRect.init(x: 0, y: 0, width: 280, height: 380))
    var groupDetailsImage : UIImageView?
    var groupDetailsLabel : UILabel?
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        if reuseIdentifier == "extraPin" {
            super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
            let width = UIScreen.main.bounds.width - 40
            let detailsView = RoundedView.init(frame: CGRect.init(x: -((width/2) - 39), y: -200, width: width, height: 200))
            groupDetailsImage = UIImageView.init(frame: CGRect.init(x: 8, y: 51, width: width - 16, height: 128))
            
            detailsView.addSubview(groupDetailsImage!)
            detailsView.backgroundColor = UIColor.white
            groupDetailsLabel = UILabel.init(frame: CGRect.init(x: 8, y: 11, width: width - 16, height: 35))
            groupDetailsLabel?.font = UIFont.init(name: "Roboto-Regular", size: 14)
            groupDetailsLabel?.textColor = UIColor.init(red: 57.0/255.0, green: 54.0/255.0, blue: 57.0/255.0, alpha: 1.0)
            
            detailsView.addSubview(groupDetailsLabel!)
            self.addSubview(detailsView)
            let imageView = CircularImage.init(frame: CGRect.init(x: 10, y: 10, width: 39, height: 39))
            imageView.contentMode = .scaleAspectFit
            self.addSubview(imageView)
            self.bringSubview(toFront: imageView)
            
            imageView.layer.cornerRadius = imageView.frame.size.height / 2;
            imageView.clipsToBounds = true;
            imageView.layoutIfNeeded()
            
            userImageView = imageView
        }else{
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        let imageView = CircularImage.init(frame: CGRect.init(x: 10, y: 10, width: 39, height: 39))
        imageView.contentMode = .scaleAspectFit
        self.addSubview(imageView)
        self.bringSubview(toFront: imageView)

        imageView.layer.cornerRadius = imageView.frame.size.height / 2;
        imageView.clipsToBounds = true;
        imageView.layoutIfNeeded()

        userImageView = imageView
        }

    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
//    override func setSelected(_ selected: Bool, animated: Bool)
//    {
//        super.setSelected(false, animated: animated)
//        if(selected)
//        {
//            var titleLength = 0
//            var subTitleLength = 0
//            var locationInfo = "\n"
//            if let title = (annotation!.title)! {
//                locationInfo = title + locationInfo
//                titleLength = title.characters.count
//            }
//            if let subTitle = (annotation!.subtitle)!  {
//                locationInfo = locationInfo + subTitle
//                subTitleLength = subTitle.characters.count
//            }
//            // Do customization, for example:
//            selectedLabel.text = locationInfo
//            selectedLabel.numberOfLines = 0
//            selectedLabel.textAlignment = .center
//            
//            let selectedAttrString = NSMutableAttributedString(attributedString: selectedLabel.attributedText!)
////            selectedAttrString.addAttribute(NSFontAttributeName, value: UIFont(name: "A Jannat LT", size: 15)!, range: NSMakeRange(0, titleLength))
//            selectedAttrString.addAttribute(NSFontAttributeName, value: UIFont(name: "A Jannat LT", size: 10)!, range: NSMakeRange(titleLength+1, subTitleLength))
//            
//            selectedLabel.attributedText = selectedAttrString
//            
//            
//            var size = selectedLabel.sizeThatFits(CGSize.init(width: selectedLabel.frame.size.width, height: 9999))
//            var newFrame = selectedLabel.frame
//            newFrame.size.height = size.height
//            selectedLabel.frame = newFrame
//            
//            size = selectedLabel.sizeThatFits(CGSize.init(width: 280, height:selectedLabel.frame.size.height))
//            newFrame = selectedLabel.frame
//            newFrame.size.width = min(280, size.width)
//            selectedLabel.frame = newFrame
//            
//            selectedLabel.frame.size = CGSize(width: selectedLabel.frame.size.width+14, height: selectedLabel.frame.size.height+14)
//            
//            selectedLabel.backgroundColor = UIColor.white
//            selectedLabel.layer.borderColor = UIColor.white.cgColor
//            selectedLabel.layer.borderWidth = 2
//            selectedLabel.layer.cornerRadius = 10
//            selectedLabel.layer.masksToBounds = true
//            selectedLabel.center.x = 0.5 * self.frame.size.width;
//            selectedLabel.center.y = -0.5 * selectedLabel.frame.height;
//            self.addSubview(selectedLabel)
//        }
//        else
//        {
//            selectedLabel.removeFromSuperview()
//        }
//    }
}
