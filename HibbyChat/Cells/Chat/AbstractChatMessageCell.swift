//
//  AbstractChatMessageCell.swift
//  HippyChat
//
//  Created by Mostafa on 5/22/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage


class AbstractChatMessageCell: UITableViewCell {
    @IBOutlet weak var userProfileImage: UIImageView!
    var delegate : ChatMessageCellDelegate?
    @IBOutlet weak var sentArrowImage: UIImageView!
    @IBOutlet weak var messageHolderView: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var userLoactionLabel: UILabel!
    
    @IBOutlet weak var chatMessageLabel: UILabel!
    @IBOutlet weak var chatDateLabel: UILabel!
    
    var chatMessage : ChatMessage?
    var user : User?
    
    func getCellHeight() -> CGFloat {
        return 95
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        let gestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(openUser))
        gestureRecognizer.numberOfTapsRequired = 1
        if let profileImage = userProfileImage {
            profileImage.addGestureRecognizer(gestureRecognizer)
            profileImage.isUserInteractionEnabled = true
        }

    }
    func openUser(){
        if let usr = user {
            delegate?.openUser(usr)
        }
    }
    func bindMsg(msg: ChatMessage, user: User) {
        self.user = user
        self.chatMessage = msg
        var msgFrame = self.messageHolderView.frame
        msgFrame.size.height = AbstractChatMessageCell.heightForLabel(text: msg.message, width: msgFrame.width - 18) - 12
        self.messageHolderView.frame = msgFrame
        self.messageHolderView.layoutIfNeeded()
        
        self.layoutIfNeeded()
        self.messageHolderView.draw(self.messageHolderView.frame)
        if let profileImage = self.userProfileImage {
            if user.image_url.characters.count > 0 {
                profileImage.sd_setImage(with: URL.init(string: user.image_url), placeholderImage: user.getPlaceholderImage())
            }else{
                profileImage.image = user.getPlaceholderImage()
            }
        }
        self.userNameLabel.text = user.getDisplayName()
//        self.userLoactionLabel.text = user.getUserCity()
        self.chatDateLabel.text = msg.getDate()
        self.chatMessageLabel.text = msg.message
    }
    
    
    static func heightForLabel(text: String, width: CGFloat) -> CGFloat
    {
        let frame = CGRect(x: 0, y: 0, width: width, height:  CGFloat.greatestFiniteMagnitude)
        let label:UILabel = UILabel(frame: frame)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.attributedText = AbstractChatMessageCell.formAttributedMessageString(text: text)
        label.sizeToFit()
        
        return label.frame.height + 62 + 129.5
    }
    /*
     *  private function that forms the message attributed string with text
     */
    private static func formAttributedMessageString(text: String) -> NSMutableAttributedString
    {
        
        let message = "\(text)"
        let length = message.characters.count
        let attributedMessage = NSMutableAttributedString(
            string: message,
            attributes: nil
        )
        
        attributedMessage.addAttributes(
            [
                NSFontAttributeName : UIFont(name: "Roboto-Regular", size: 14)!,
                NSForegroundColorAttributeName : UIColor.white
            ]
            ,
            range: NSRange(location: 0, length: length)
        )
        return attributedMessage
    }

    
}
