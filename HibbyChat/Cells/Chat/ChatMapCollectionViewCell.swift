//
//  ChatMapCollectionViewCell.swift
//  HippyChat
//
//  Created by Mostafa on 6/6/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit

protocol MapCellDelegate {
    func openIndexPath(indexPath: IndexPath)
}
class ChatMapCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var userImageView: CircularImage!
    var indexPath : IndexPath = IndexPath()
    var delegate : MapCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        let gestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(openUser))
        gestureRecognizer.numberOfTapsRequired = 1
        self.userImageView.addGestureRecognizer(gestureRecognizer)
        self.userImageView.isUserInteractionEnabled = true

    }
    func openUser(){
        print("Open User")
        self.delegate?.openIndexPath(indexPath: self.indexPath)
    }
}
