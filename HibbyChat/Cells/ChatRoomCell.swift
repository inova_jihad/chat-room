//
//  ChatRoomCell.swift
//  HippyChat
//
//  Created by Mostafa on 5/16/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage



class ChatRoomCell: UITableViewCell {
    @IBOutlet weak var chatRoomImageView: CircularImage!
    @IBOutlet var hideChatRoomImage: UIImageView!
    @IBOutlet var chatMsgLabel: RoundedLabel!
    @IBOutlet weak var chatRoomTitleLabel: UILabel!
    @IBOutlet weak var chatRoomOwnerName: UILabel!
    @IBOutlet weak var chatRoomDateLabel: UILabel!
    
    var indexPath : IndexPath = IndexPath.init(row: 0, section: 0)
    var delegate : ChatRoomCellDelegate?
    var owner : User = User()
    var chatRoom : ChatRoom = ChatRoom()

    override func awakeFromNib() {
        super.awakeFromNib()
        let gestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(openUser))
        gestureRecognizer.numberOfTapsRequired = 1
        self.chatRoomImageView.addGestureRecognizer(gestureRecognizer)
//        let gestureRecognizerHideHibby = UITapGestureRecognizer.init(target: self, action: #selector(hideHibby))
//        gestureRecognizerHideHibby.numberOfTapsRequired = 1
//        self.hideHibbyImage.addGestureRecognizer(gestureRecognizerHideHibby)

    }
    
//    func hideHibby(){
////        self.delegate?.hideGroup(group)
//    }
    
    func openUser(){
        delegate?.openUser(owner)
    }
    
    
    func bindChatRoom(chatRoom: ChatRoom){
        self.chatRoom = chatRoom
        print(chatRoom.image_url)

        owner = User()
        for user in chatRoom.users {
            if user.id == chatRoom.owner_id {
                owner = user
                break;
            }
        }
        self.chatRoomImageView?.sd_setImage(with: URL(string: owner.image_url), placeholderImage: owner.getPlaceholderImage())

//        self.hippyOwnerName.text = owner.username
        if UserManager().loadCachedUser() != nil {
//            if !group.is_root && group.owner_id == user.id {
//                self.hippyTitleLabel.text = "\(group.title) (Resent)"
//            }else{
                self.chatRoomTitleLabel.text = "\(chatRoom.title)"

        }else{
            self.chatRoomTitleLabel.text = "\(chatRoom.title)"

        }
        self.chatRoomDateLabel.text = chatRoom.getDate()
        self.chatMsgLabel.text = " \(chatRoom.unreadCount) "
        self.chatMsgLabel.isHidden = chatRoom.unreadCount==0
        self.chatRoomTitleLabel.sizeToFit()
        self.chatMsgLabel.sizeToFit()
    }
}
