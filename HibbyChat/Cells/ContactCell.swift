//
//  ContactCell.swift
//  Hippy
//
//  Created by Mostafa on 5/16/17.
//  Copyright © 2017 Inova. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage



class ContactCell: UITableViewCell {
    
    @IBOutlet weak var userImageView: CircularImage!
    @IBOutlet weak var selectionStateButton: UIButton!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    var delegate : ContactCellDelegate?
    var indexPath :IndexPath?
    var addContact : Bool = false
    var user: User?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let gestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(openUser))
        gestureRecognizer.numberOfTapsRequired = 1
        self.userImageView.addGestureRecognizer(gestureRecognizer)
        self.userImageView.isUserInteractionEnabled = true

    }
    
    func bindUser(user:User, selected: Bool, indexPath: IndexPath){
        self.user = user
        self.indexPath = indexPath
        self.addContact = selected
        setButtonImage()
        
//        if user.userType == .contact, let image = user.image {
//            self.userImageView.image = image
//        }else{
            self.userImageView.sd_setImage(with: URL(string: user.image_url), placeholderImage: user.getPlaceholderImage())
//        }
        
//        self.cityLabel.text = user.getUserCity()
        self.usernameLabel.text = user.getDisplayName()
    }
    
    @IBAction func selectContact(_ sender: UIButton) {
        addContact = !addContact
        setButtonImage()
        self.delegate?.didSelectContact(indexPath: indexPath!, selected: addContact)
    }
    
    func setButtonImage(){
        if addContact {
            self.selectionStateButton.setImage(UIImage.init(named: "icCheckList"), for: .normal)
        }else{
            self.selectionStateButton.setImage(UIImage.init(named: "btnAdd"), for: .normal)
        }
    }
    
    func openUser(){
        if let usr = self.user {
            self.delegate?.openUser(user: usr)
        }
    }
}
